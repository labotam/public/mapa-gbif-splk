#
# This is the server logic of a Shiny web application. You can run the
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#
#library(shiny)
dir.create("gbif",showWarnings = FALSE)
dir.create("taxized",showWarnings = FALSE)
dir.create("maps",showWarnings = FALSE)
dir.create("maps_pngs",showWarnings = FALSE)
dir.create("temp",showWarnings = FALSE)

#tropicos_ping(what = "status")

# Define server logic required to draw a histogram
shinyServer(function(input, output) {
 
  # IMPORT BATCH NAMES ####
 observeEvent(input$batchNames, {
  output$nomevalido = NULL
  output$statusnome = NULL
  output$plotmap <- NULL
  output$general <- renderUI({
   tagList(
    h4("Importar uma lista de nomes"),
    helpText("Importe uma lista de nomes. Irá validar a taxonomia e adicionar à lista de espécies. Se quiser que também já baixe os dados de ocorrência, selecione a opção abaixo."),
    pre("
    Um nome por linha:
    
  Aldina heterophylla
  Ocotea floribunda
  Schopfia clarkii
  Micropholis guyanensis duckeana
     " ),
    checkboxInput('batchOccurrencesQuery',label = span("Busca dados de ocorrência?",class= 'mynotered')),
    textAreaInput('namesToImport',label='Lista de nomes',rows=10,resize='both'),
    br(),
    uiOutput('importNamesListStatus'),
    actionButton('submitNamesList',label = 'Enviar',class="buttongreen"),
    br(),br(),
    uiOutput('importLog')
   )
  })
 })
 
 observeEvent(input$submitNamesList,{
    names.list = input$namesToImport
    output$statusnome = NULL
    output$plotmap <- NULL
    
    #busca tambem?
    busca.occ = input$batchOccurrencesQuery
    
    #names.list = "  Schoepfia clarkii\n  "
    names.list = sapply(strsplit(names.list,"\n")[[1]],mytrim)
    names.list = unique(names.list[!names.list%in%""])
    resultado = NULL
    
    houveerro = NULL
    withProgress(message = 'Importando os nomes', value = 0, {
     
    for(i in 1:length(names.list)) {
     erro =1
     i.name = names.list[i]
     incProgress(1/length(names.list), detail = i.name)
     fntax = paste("taxized/",nometofile(i.name),".json",sep = "")
     if (file.exists(fntax)) {
      msg = "Já existe o registro!"
      erro =0
     } else {
       tz = taxize.nome(i.name)
       msg = "Falhou. Não encontrado online"
       if (tz$salva) {
        msg = 'OK. Adicionado'
        sinotb = try(pegasinonimos(tz),silent = T)
        if (class(sinotb)%in%"try-error") {
          msg = "OK, mas não foi possivel validar sinonimos. Fazer manualmente."
          sinotb= NULL
        }
        taxdados = list(tz=tz,sinotb=sinotb)
        #salva o dado nomenclatural
        write(toJSON(x=taxdados),file=fntax)
        #adicona o nome a lista de nomes usados
        if (file.exists("nomesusados.json")) {
         osnomes = fromJSON(file="nomesusados.json")
         osnomes = unique(c(tz$nome, osnomes))
        } else {
          osnomes = tz$nome
        }
        write(toJSON(osnomes),file="nomesusados.json")
        
        #PROCURA POR DADOS ONLINE
        if (busca.occ) {
          msg.occ = " && Ocorrências existentes"
          file_dados = paste("gbif/",nometofile(tz$nome),"_all.json",sep = "")
          if (!file.exists(file_dados)) {
              gbif.dad = getOccurrencesValidating(tz$nome,th.shape = map.shape)
              msg.occ = " && Ocorrências não encontradas."
              if (!is.null(gbif.dad)) {
               msg.occ =  paste(" && ", nrow(gbif.dad),'ocorrências encontradas.')
               write(toJSON(gbif.dad), file=file_dados)
              }
          }
          msg = paste(msg,msg.occ)
       }
       }
     }
     resultado = c(resultado,msg)
    }  
   }) 
   resultado = paste(names.list,resultado,sep="  >> ",collapse = "\n")  
    output$importLog <- renderUI({
     tagList(
      pre(resultado)
     )
   }) 
   osnomes = fromJSON(file="nomesusados.json")
   updateSelectizeInput(inputId="nome", label="Qual o nome da espécie?", choices=osnomes,selected = NULL, options = list(create = TRUE), server = FALSE) 
 })
 
 
  # GET GBIF ####
  observeEvent(input$getGBIF, {
      if (!is.null(input$nome) & !input$nome%in%"") {
          fn = paste("gbif/",nometofile(input$nome),"_all.json",sep = "")
          #fn2 = paste("gbif/",nometofile(input$nome),"_valid.json",sep = "")
          if (!file.exists(fn) | input$baixagbif) {
            #get data for name (and sinonims se houver baixado isso)
            #input = list(nome='Aldina heterophylla')
            gbif.dad = getOccurrencesValidating(input$nome,th.shape = map.shape)
            cls = "mynotered"
            msg = "Dados não encontraods"
            if (!is.null(gbif.dad)) {
                tg = table(gbif.dad$source)
                msgg = paste(names(tg),tg,sep=": ",collapse = " and ")
                msg = paste(nrow(gbif.dad),'total registros encontrados: ',msgg)
                write(toJSON(gbif.dad), file=fn)
                #write.table(gbif.dad,file=fn,sep='\t',na="",quote=T,row.names = F)
                if (sum(gbif.dad$InAmerica)>0) {
                   cls = "mynotegreen"
                   msg = paste(msg," [",sum(gbif.dad$InAmerica),' tem coordenadas válidas ]')
                } else {
                   msg = paste(msg,"[ SEM COORDENADAS OU INVALIDAS ]")
                }
                output$general <- renderUI({
                  tagList(
                    span(msg, class=cls),
                    downloadButton("gbifdataall", "Dados GBIF-SpeciesLink"),
                  )
                })
            
          }
            else {
                output$general <- renderUI({
                  tagList(
                    span(msg, class=cls),
                  )
                })
            }
          }
          else {
            if (file.exists(fn)) {
            output$general <- renderUI({
              tagList(
                span("Já existem arquivos GBIF baixados:", class="mynotegreen"),
                br(),
                downloadButton("gbifdataall", "Dados GBIF-SpeciesLink")
                )
              })
            } else {
                output$general <- renderUI({
                  tagList(
                    span("Dados não encontrados", class="mynotered")
                  )
                })
            }
          }
      } else {
       msg = "Precisa digitar um nome de especie para poder ver ou pegar dados GBIF"
       output$statusnome <- renderUI({
        tagList(
         span(msg, class="myalertred")
        )
       })
      }
   })

  # download GBIF DATA all ####
  output$gbifdataall <- downloadHandler(
    filename = function() {
      paste(gsub(" ","_",input$nome), "_GBIFdata_all.csv", sep = "")
    },
    content = function(file) {
      #fn =
      #dz = read.table(file=fn,header=T,sep="\t",as.is=T,na.strings = c("NA",""),stringsAsFactors = F)
      #file.copy("out.zip", file)
      fn= paste("gbif/",nometofile(input$nome),"_all.json",sep = "")
      dz = fromJSON.dataframe(fn)
      write.csv(dz, file, row.names = FALSE)
    }
  )

  # download GBIF data valid ####
  output$gbifdata <- downloadHandler(

    filename = function() {
      paste(gsub(" ","_",input$nome), "_GBIFdata_valid.csv", sep = "")
    },

    content = function(file) {
      #fn2 = paste("gbif/",gsub(" ","_",input$nome),"_valid.json",sep = "")
      #dz = read.table(file=fn2,header=T,sep="\t",as.is=T,na.strings = c("NA",""),stringsAsFactors = F)
      fnn = paste("gbif/",nometofile(input$nome),"_all.json",sep = "")
      #print(fnn)
      dz = fromJSON.dataframe(fnn)
      if (sum(dz$InAmerica==1)>0) {
        dz = dz[dz$InAmerica==1,]
      } else {
        dz = "Nenhum registro com coordenada válida"
      }
      #print(str(dz))
      write.csv(dz, file, row.names = FALSE)
    }
  )
  
  # download TaxNames data ####
  output$taxdata <- downloadHandler(
    filename = function() {
      paste(nometofile(input$nome), "_TNRS_data.csv", sep = "")
    },
    content = function(file) {
      fntax = paste("taxized/",nometofile(input$nome),".json",sep = "")
      ll = fromJSON.dataframe.tax(fntax)
      sinotb = ll$sinotb
      ipni = sinotb$ipni
      ipni$name_status = 'Valid'
      sino = sinotb$ipni.sino
      if (!is.null(sino)) {
        sino$name_status[is.na(sino$name_status) | sino$name_status==""] = "Synonym"
        ipni = rbind(ipni,sino)
      }
      #print(str(dz))
      write.csv(ipni, file, row.names = FALSE)
    }
  )

  # Validate Taxonomic Name ####
  observeEvent(input$taxizeit, {
      #verifica se o nome foi enviado
        cls = "myalertred"
        if (!is.null(input$nome) & !input$nome%in%"") {
          fntax = paste("taxized/",nometofile(input$nome),".json",sep = "")
          sinotb = list()
          if (!file.exists(fntax) | input$redotax) {
            #valida o nome
            tz = taxize.nome(input$nome)
            if (tz$salva) {
              sinotb = pegasinonimos(tz)

              taxdados = list(tz=tz,sinotb=sinotb)

              write(toJSON(x=taxdados),file=fntax)

              if (file.exists("nomesusados.json")) {
                osnomes = fromJSON(file="nomesusados.json")
                osnomes = unique(c(tz$nome, osnomes))
              } else {
                osnomes = tz$nome
              }
              write(toJSON(osnomes),file="nomesusados.json")

            }
          } else {
            ll = fromJSON.dataframe.tax(fntax)
            tz = ll$tz
            sinotb = ll$sinotb
          }
          msg = tz$msg
          if (tz$salva) {
            msg.sino = "Sinônimos não encontrados"
            if (!is.null(sinotb$ipni.sino)) {
              if (nrow(sinotb$ipni.sino)>0) {
                sinohtml = apply(sinotb$ipni.sino,1,italicize_name_html,includerefs = input$nomecomrefs)
                names(sinohtml) = NULL
                msg.sino =NULL
              }
            }
            taghtml = apply(sinotb$ipni,1,italicize_name_html,includerefs = input$nomecomrefs)
            output$nomevalido <- renderText({
                paste(taghtml,collapse = "<br>")
            })

            output$general <- renderUI({
              tagList(
                h4("Sinônimos"),
                br(),
                htmlOutput('ossinonimos',class='sinonimos')
              )
            })
            output$ossinonimos <- renderText({
              if (is.null(msg.sino)) {
                paste(sinohtml,collapse = "<br>")
              } else {
                paste(span(msg.sino,class=cls))
              }
            })
            cls = "myalert"
          }
          output$statusnome <- renderUI({
           tagList(
            span(msg, class=cls),
            br(),
            hr(),
            downloadButton("taxdata", "Baixar resultado da busca taxonômica")
           )
          })
        } else {
          msg = "Precisa digitar um nome de especie para poder buscar em iPlant_TNRS e Tropicos"
          output$statusnome <- renderUI({
           tagList(
            span(msg, class=cls),
           )
          })
        }
        
  })
  
  # make the Map ####
  addResourcePath("maps", paste(getwd(),"/maps",sep=""))
  observeEvent(input$mapit,{
    output$statusnome = NULL
    output$plotmap <- NULL
    output$general <- NULL
    if (!is.null(input$nome) & !input$nome%in%"") {
      fnmap= paste("maps/",nometofile(input$nome),"_",input$mapsize,".pdf",sep="")
      if (input$mapredoit | !file.exists(fnmap)) {
      fn2 = paste("gbif/",nometofile(input$nome),"_all.json",sep = "")
      if (!file.exists(fn2)) {
        msg = "Precisa baixar dados do GBIF e validar"
        cls = 'myalertred'
        output$general <- renderUI({
          tagList(
            span(msg,class=cls)
          )
        })
      }
      else {
        dz = fromJSON.dataframe(fn2)
        if (input$mapalsoinvalid) {
         temcoords = sum(dz$toplot==1 |dz$hascoords==1)>0
        } else {
         temcoords = sum(dz$toplot==1)>0
        }
        #msg = paste("Tem ",nrow(dz),"registros e é",class(dz),"aqui:",input$mapredoit)
        if (!temcoords) {
           msg = "Os dados não contém coordenadas válidas, i.e. que caem dentro do continente americano"
           cls = 'myalertred'
           output$general <- renderUI({
             tagList(
               span(msg,class=cls)
             )
           })
        } else { 
        #faz o mapa
        fnmap= paste("maps/",nometofile(input$nome),"_",input$mapsize,".pdf",sep="")
        msg = "A imagem foi gerada com sucesso"
        fazomapa(minput=input,map.srtm=map.srtm,includeinvalid=input$mapalsoinvalid)
        cls = 'mynotegreen'
        output$general <- renderUI({
          tagList(
            span(msg,class=cls),
            br(),
            downloadButton("downloadimgs", "Baixar todos os arquivos gerados"),
            br(),
            br(),
            imageOutput('plotmap')
          )
        })
        fnimg= paste("maps_pngs/",nometofile(input$nome),"_",input$mapsize,".png",sep="")
        output$plotmap <- renderImage({
         list(src = fnimg, alt = "Não achei a imagem",width=600)
        }, deleteFile = FALSE)
        }
      }
      } else {
        msg = "Esse mapa já existia. Mude a opção se quiser regerar"
        cls = 'mynotegreen'
        fnmap= paste("maps/",nometofile(input$nome),"_",input$mapsize,".pdf",sep="")
        output$general <- renderUI({
           tagList(
            span(msg,class=cls),
            br(),
            downloadButton("downloadimgs", "Baixar as imagens geradas em PDF"),
            br(),
            br(),
            imageOutput('plotmap')
           )
        })
        fnimg= paste("maps_pngs/",nometofile(input$nome),"_",input$mapsize,".png",sep="")
        output$plotmap <- renderImage({
              list(src = fnimg, alt = "Não achei a imagem",width=600)
        }, deleteFile = FALSE)
      }





    } else {
      msg = "Precisa digitar um nome de espécie para poder mapear"
      cls = 'myalertred'
      output$statusnome <- renderUI({
        tagList(
          span(msg,class=cls)
        )
      })
    }

  })

  # download Maps ####
  output$downloadimgs <- downloadHandler(
    filename = function() {
      paste(gsub(" ","_",input$nome), "_Mapas.zip", sep = "")
    },
    content = function(fname) {
      #fn2 = paste("gbif/",gsub(" ","_",input$nome),"_valid.json",sep = "")
      #dz = read.table(file=fn2,header=T,sep="\t",as.is=T,na.strings = c("NA",""),stringsAsFactors = F)
      g1 = grep(nometofile(input$nome),dir("maps"),ignore.case = T)
      asimgs = paste("maps/",dir("maps")[g1],sep="")
      torm = paste("temp/",dir("temp"),sep="")
      if (length(dir("temp"))>0) {
        file.remove(torm)
      }
      asimgstemp = paste("temp/",dir("maps")[g1],sep="")
      file.copy(from=asimgs,to=asimgstemp, copy.date = TRUE)
      tempf = paste("temp/",nometofile(input$nome),"_GBIFvalid.csv",sep = "")
      fnn = paste("gbif/",nometofile(input$nome),"_valid.json",sep = "")
      dz = fromJSON.dataframe(fnn)
      #print(str(dz))
      write.csv(dz, tempf, row.names = FALSE)

      tempf = paste("temp/",nometofile(input$nome),"_GBIFall.csv",sep = "")
      fnn = paste("gbif/",nometofile(input$nome),"_all.json",sep = "")
      dz = fromJSON.dataframe(fnn)
      #print(str(dz))
      write.csv(dz, tempf, row.names = FALSE)


      fntax = paste("taxized/",nometofile(input$nome),".json",sep = "")
      ll = fromJSON.dataframe.tax(fntax)
      sinotb = ll$sinotb
      ipni = sinotb$ipni
      ipni$name_status = 'Valid'
      sino = sinotb$ipni.sino
      if (!is.null(sino)) {
        sino$name_status[is.na(sino$name_status) | sino$name_status==""] = "Synonym"
        ipni = rbind(ipni,sino)
      }
      tempf = paste("temp/",nometofile(input$nome),"_TNRS_IPNI.csv",sep = "")
      write.csv(ipni, tempf, row.names = FALSE)

      asimgs = dir("temp")[grep(nometofile(input$nome),dir("temp"),ignore.case = T)]
      path = paste(getwd(),"temp",sep="/")
      setwd(path)
      zip(zipfile=fname, files=asimgs)
    },
    contentType = "application/zip"
  )

  # edit Occurrences ####
  # NOT IN USE OLD ###
  observeEvent(input$anotatetoplot,{
   s = input$tbTagRecords_rows_selected
   msg = "Precisa selecionar linhas"
   clas = 'mynotered'
   if (!is.null(s) & length(s)>0) {
      #pega os dados e atualiza a coluna toplot
      fn = paste("gbif/",nometofile(input$nome),"_all.json",sep = "")
      dados = fromJSON.dataframe(fn)
      #rownames(dados) = paste('r',1:nrow(dados),sep="")
      #v1 = dados$InAmerica==1
      #anota a mudanca
      old = dados$toplot[s]
      std = c("0" = 1, "1" = 0)
      new = std[as.character(old)]
      dados$toplot[s] = new
      #salva o arquivo atualizado
      write(toJSON(dados), file=fn)
      msg = "Atualizado com sucesso"
      clas = 'mynotegreen'
      #atualiza o data table
      clsshow = colnames(dados)
      clsshow = standard.columns[standard.columns%in%clsshow]
      clstypes = standard.input.types[standard.columns%in%clsshow]
      #define datable options
      current.year = as.numeric(format(Sys.Date(),"%Y"))
      years = c(current.year:2000)
      choices = list('toplot'=c(0,1),'year'=years,'day'=c(1:31),'month'=c(1:12))
      
      
      output$tbTagRecords =  DT::renderDataTable({dados[,clsshow]}, server = TRUE,filter = "top",rownames=F, options = list(5),selection=list(mode = 'multiple',target = 'row'))
   }
   #coloca a mensagem
   output$gbifrecsStatus <-renderUI({
    tagList(
     span(msg,class=clas)
    )
   })
  })
  
  # edit ocurrences DATATABLE ####
  observeEvent(input$tagrecords,{
   output$statusnome = NULL
   output$plotmap <- NULL
   output$general<-NULL
   fn = paste("gbif/",nometofile(input$nome),"_all.json",sep = "")
   if (file.exists(fn)) {
    output$general <- renderUI({
     tagList(
      h4("Editar os dados"),
      helpText("Todas as linhas com coordenadas válidas são automáticamente marcadas para inclusão no mapa e por isso tem valor 1 na coluna 'toplot'."),
      br(),
      uiOutput('gbifrecsStatus'),
      #br(),
      #actionButton("anotatetoplot",label = "Esconder/Mostrar",class='buttongreen'),
      #br(),
      br(),
      uiOutput("tbTagRecords")
      #DT::dataTableOutput('tbTagRecords')
     )
    })
     dados = fromJSON.dataframe(fn)
     if (is.data.frame(dados) & ncol(dados)>1) {
       rownames(dados) = paste('r',1:nrow(dados),sep="")
       #v1 = dados$InAmerica==1
       #dados = dados[v1,]
       clsshow = colnames(dados)
       clsshow = standard.columns[standard.columns%in%clsshow]
       clstypes = standard.input.types[standard.columns%in%clsshow]
       #define datable options
       current.year = as.numeric(format(Sys.Date(),"%Y"))
       years = c(current.year:2000)
       input.choices = list('toplot'=c(0,1),'year'=years,'day'=c(1:31),'month'=c(1:12))
       
       labotam.dtEdit(input, output,
              name = 'tbTagRecords',
              thedata = dados,
              input.types = clstypes,
              view.cols = clsshow,
              edit.cols = clsshow,
              input.choices = input.choices,
              selectize = TRUE,
              modal.size = "l",
              show.delete = TRUE,
              show.update = TRUE,
              show.insert = TRUE,
              show.copy = FALSE,
              click.time.threshold = 2,
              callback.update = ocorrenciasUpdate,
              callback.insert = ocorrenciasInsert,
              callback.delete = ocorrenciasDelete,
              datatable.options = list(pageLength = 10)
       )
     } else {
      msg = "Dados inválidos. Baixe novamente."
      cls = 'myalertred'
      output$gbifrecsStatus <- renderUI({
       tagList(
        span(msg,class=cls)
       )
      })
     }
     #ql = which(dados$toplot==1)
     #selected=ql,
     #output$tbTagRecords =  DT::renderDataTable({dados[,clsshow]}, server = TRUE,filter = "top",rownames=F, options = list(pageLength = 5),selection=list(mode = 'multiple',target = 'row'))
   } else {
    msg = "Não existem dados baixados para esse taxon para poder ver"
    cls = 'myalertred'
    output$statusnome <- renderUI({
     tagList(
      span(msg,class=cls)
     )
    })
   }
   
  })

  observeEvent(input$about,{
   output$general <- renderUI({
    includeMarkdown("README.md")
   })
   
   
  })
})
