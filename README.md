# Labotam - Mapeia GBIF-SpeciesLink

Este é um pequeno aplicativo para gerar mapas de ocorrência de espécies de plantas (da América do Sul) para apresentações e outros documentos. Basta você digitar um nome científico e com alguns cliques gera duas versões do mapa, uma em **PDF** outra em **PNG**. A interface tem alguma flexibilidade de edição, mas os dados de ocorrência baixados e as figuras são salvos em arquivos que você pode manipular diretamente no R para fazer as modificações que quiser. 

Referir aos portais dos repositórios [GBIF](https://www.gbif.org/citation-guidelines) e [SpeciesLink API](https://api.splink.org.br/) para como citar os dados.

### O que faz?

1. Valida o **nome científico** usando o pacote Taxize e a API do iPlant_TNRS, buscando sinônimos em [Tropicos.org](www.tropicos.org);
1. Checa por sinônimos e baixa os autores e referências bibliográficas dos nomes encontrados;
1. Baixa dados do [GBIF](https://www.gbif.org/citation-guidelines) e do [SpeciesLink API](https://api.splink.org.br/) para os nomes científico (o buscado também pelos sinônimos)
1. Valida os dados:  
    - anota se coordenadas são válidas (latitudes e longitudes tem valores válidos)
    - anota se a coordenada coloca o ponto dentro do continente Americano (modifique o código para cobrir outras áreas)
1. Permite esconder, editar ou inserir manualmente dados
1. Permite gerar mapa de distribuição com algum controle de formatação, usando como pano de fundo um mapa de elevação do terreno para América do Sul, conforme exemplo abaixo.

## Dados e figuras gerados
Todos os arquivos ficam organizados na pasta do aplicativo, que tem a seguinte estrutura:

1. Arquivos e pastas com os dados baixados:
 * **gbif/..** - contém os dados das ocorrências em formato **json** com o nome buscado (e.g. aldina\_heterophylla\_all.json) com todos os registros encontrados em [GBIF](https://www.gbif.org/citation-guidelines) e [SpeciesLink](http://splink.cria.org.br), e anotações se a coordenada é válida ou não;
 * **maps/..** - contém os mapas gerados em formato vetorial **PDF**
 * **maps_png/..** - contém as mesmas figuras que em maps, mas em formato raster **PNG** 
 * **nomesusados.json** - contém apenas a lista de nomes buscados para aparecer no menu
 * **taxized** - contém os dados nomenclaturais para cada nome buscado, em formato **json**
 
2. Arquivos e pastas do aplicativo
 * **funcoes.R** - contém funções que buscam, validam e mapeiam os dados de ocorrências (código não padronizado, não documentado, mas se fácil de entender o que fazem)
 * **countries_shp/..** - contém o mapa utilizado para validar as coordenadas 
 * **prep_scripts** - contém os scripts utilizados para baixar e juntar os arquivos de elevação para a América do Sul das figuras (ver função __getData()__ do pacote __raster__ para fonte dos dados)  
 * **srtmAmericaTropical.grd e .gri ** - o arquivo raster de elevação para o recorte América Tropical gerado com script acima

## Instalar

1. Precisa do [R](https://cran.r-project.org/) e do [RStudio](https://www.rstudio.com/). Este aplicativo é gerado com o pacote [Shiny](https://shiny.rstudio.com/). 
1. Clonar este repositório no seu computador ou fazer o download e salvar a pasta onde quiser;
1. Instalar vários pacotes (veja lista no arquivo **instalar-pacotes.R**) - certifique-se que todos os pacotes estão funcionando antes de usar. 
1. Pronto, pode usar

## Usar

1. Abra o arquivo **Ui.R** no RStudio e execute o aplicativo, ou execute o aplicativo sem abrir o arquivo usando (`runApp("caminho/do/Ui.R")`)
1. Digite um nome de espécie qualquer e siga os botões verdes de cima para baixo até gerar um mapa (ajuste o tamanho e as cores)
1. Pode editar os dados no caminho
1. O nome digitado ficará memorizado no menu.
1. Você pode importar uma lista de nomes também se precisa de muitos mapas

## Manipulando os dados baixados no R
Você pode editar os arquivos com os dados de ocorrência diretamente no R. Adicionar, remover ou editar registros e fazer o mapa novamente.

**Atenção**: editar os arquivos, cuidado para não pedir para baixar novamente os dados, pois os dados existentes serão sobrepostos. 

Durante a validação, três colunas são adicionadas aos dados baixados do Gbif e SpeciesLink. As colunas são:
* **toPlot** - 0 ou 1 indicando se (pode) inclui (1) o registro no mapa (pode editar o valor)
* **InAmerica** - 0 ou 1, se a coordenada cai dentro do poligono informado no arquivo shapefile na pasta **country_shp**.
* **hascoords** - 0 ou 1, se o registro tem uma coordenada válida, ou seja, se os valores de latitudes e longitudes são válidos

Os arquivos são salvos em formato [Json](https://pt.wikipedia.org/wiki/JSON) nas pastas indicadas acima (ocorrências em **gbif/** e nomenclatura em **taxized/**). Para ler os arquivos e modificar os dados, siga o seguinte exemplo:

```
source(funcoes.R)

#ARQUIVO OCORRENCIAS
arquivo = 'gbif/aldina_heterophylla_all.json
dados =  fromJSON.dataframe(arquivo) #dados de ocorrencia

    
#edite os dados como quiser, adicionando ou removendo registros.
...

#salve os dados em json
write(toJSON(dados,file=arquivo))

#ARQUIVO TAXONOMICO 
#substitua acima por
dados = fromJSON.data.frame.tax(arquivo)
```

## Bugs

1. Se sua internet for lenta, pode não funcionar
2. Se houver muitos dados, **pode demorar para baixar os dados**, seja paciente, o limite de tempo de busca é 120 segundos e o limite de registros é 10.000.

## Exemplo da interface e do mapa

![Exemplo da Interface](www/screenSample.png)

## Como citar

```
@Manual{,
  title = {Labotam - Mapeia GBIF-SpeciesLink - aplicativo para mapas rápidos do GBIF e SpeciesLink},
  author = {Alberto Vicentini},
  year = {2021},
  version = {0.2},
  note = {Laboratório de Botânica Amazônica - Instituto Nacional de Pesquisas da Amazônia INPA, Manaus, Brasil},
  url = {https://gitlab.com/labotam/public/mapa-gbif-splk},
  downloaded = {DATA ATUAL}
}
```

## Licensa

**MIT License**

Copyright (c) 2021 Alberto Vicentini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
